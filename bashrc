#!/bin/bash
# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

HISTSIZE=
HISTFILESIZE=

shopt -s autocd
shopt -s cdspell
shopt -s checkwinsize
shopt -s globstar
shopt -s histappend
shopt -s checkjobs
shopt -s direxpand

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi


# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

dkprompt() {
    status=$?

    orange="\033[1;31m"
    gray="\033[1;33m"
    cyan="\033[0;36m"
    reset="\033[0;0m"

    p_status="\[${reset}\]s\[${cyan}\]${status}\[${reset}\]"
    if [ $status -ne 0 ]
    then
        p_status="\[${reset}\]s\[${orange}\]${status}\[${reset}\]"
    fi

    p_jobs="\[${reset}\]j\[${cyan}\]\j\[${reset}\]"
    p_hist="\[${reset}\]!\[${cyan}\]\!\[${reset}\]"
    p_dir="\[${reset}\]\W\[${reset}\]"
    p_sigil="\[${orange}\]$\[${reset}\]"
    p_open="\[${orange}\][\[${reset}\]"
    p_close="\[${orange}\]]\[${reset}\]"

    export PS1="${p_open} ${p_jobs} ${p_status} ${p_close} ${p_dir} ${p_sigil} "
}

PROMPT_COMMAND="dkprompt"

alias vi="vim -p"
alias ls="ls --classify --color=always"

export GOPATH="$HOME/go"
export GCC_COLORS="error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01"
export VISUAL="vim"
export EDITOR="vim"

export PATH=\
/usr/local/sbin:\
/usr/local/bin:\
/usr/sbin:\
/usr/bin:\
/sbin:\
/bin:\
/snap/bin:\
$GOPATH/bin

