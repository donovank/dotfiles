" Donovan Keohane 2019
" -- vimrc -----------

" Move and down by one graphical cell.
nnoremap j gj
nnoremap k gk

" Only allow escaping through 'jk'
inoremap <ESC> <NOP>
inoremap jk    <ESC>

" Unbind arrow keys
noremap  <Up>    <Nop>
noremap  <Down>  <Nop>
noremap  <Left>  <Nop>
noremap  <Right> <Nop>
inoremap <Up>    <Nop>
inoremap <Down>  <Nop>
inoremap <Left>  <Nop>
inoremap <Right> <Nop>

" Fix hasty typings of w, q, and wq.
cnoremap Q  q
cnoremap qw wq
cnoremap qW wq
cnoremap Qw wq
cnoremap wQ wq
cnoremap Wq wq
cnoremap WQ wq
cnoremap W  w

" Have search results appear centered.
nnoremap n nzz

" ---------------------------------------------------- "

let mapleader=" "

" Tab management
nnoremap <Leader>H  :tabmove -1<CR>
nnoremap <Leader>h  :tabprevious<CR>
nnoremap <Leader>L  :tabmove +1<CR>
nnoremap <Leader>l  :tabnext<CR>
nnoremap <Leader>n  :tabnew<CR>
nnoremap <Leader>q  :tabclose<CR>

" On the go vimrc editing
nnoremap <Leader>ev :tabnew $MYVIMRC<CR>
nnoremap <Leader>sv :source $MYVIMRC<CR>

" Misc
nnoremap <Leader>f :CtrlPCurWD<CR>
nnoremap <Leader>P :set paste!<CR>
nnoremap <Leader>G :GitGutterToggle<CR>

" ---------------------------------------------------- "

call plug#begin("~/.vim/plugged")
Plug 'airblade/vim-gitgutter'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'itchyny/lightline.vim'
Plug 'junegunn/seoul256.vim'
Plug 'sheerun/vim-polyglot'
Plug 'tpope/vim-sensible'
call plug#end()

" ---------------------------------------------------- "

set backspace=indent,eol,start
set copyindent
set cursorline
set emoji
set encoding=utf8
set expandtab
set hidden
set hlsearch
set incsearch
set laststatus=2
set nobackup
set nocompatible
set noerrorbells
set noshowmode
set noswapfile
set nowrap
set number
set prompt
set relativenumber
set shiftwidth=4
set showmatch
set signcolumn=auto
set so=7
set splitright
set tabstop=4
set title
set undodir=~/.vim/undo
set undofile
set visualbell
set wildmenu
set wildmode=longest:full,full
set wrap linebreak nolist

syntax on
colorscheme seoul256

filetype plugin indent on

" ---------------------------------------------------- "

let g:lightline = {
\ 'colorscheme': 'seoul256',
\   'active': {
\       'left': [ 
\           [ 'mode', 'paste' ],
\           [ 'readonly', 'filename' ]
\       ],
\   },
\ }

" ---------------------------------------------------- "

if has("autocmd")
    au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif
